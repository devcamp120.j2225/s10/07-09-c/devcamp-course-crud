// Import thư viên mongooseJs
const mongoose = require("mongoose");

// Khai báo class Schema của thư viện mongooseJs
const Schema = mongoose.Schema;

// Khai báo course schema
const courseSchema = new Schema({
    title: {
        type: String,
        required: true,
        unique: true
    },
    description: {
        type: String,
        required: false
    },
    noStudent: {
        type: Number,
        default: 0
    },
    reviews: [
        {
            type: mongoose.Types.ObjectId,
            ref: "reviews"
        }
    ]
}, {
    timestamps: true
});

module.exports = mongoose.model("courses", courseSchema);